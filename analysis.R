library(tidyverse)
cars_data<-read.csv('carsdata-cars-data-QueryResult.csv')
names(cars_data)

#shapiro-wilk Normality Test
shapiro.test(cars_data$Weight)
shapiro.test(cars_data$Mileage)


# Shapiro-Wilk normality test

# data:  cars_data$Weight
# W = 0.94136, p-value = 1.97e-11

# data:  cars_data$Mileage
# W = 0.96797, p-value = 1.183e-07

#Corelation test
kend_cor<-cor.test(cars_data$Weight,cars_data$Mileage,method = "kendall")
spear_cor<-cor.test(cars_data$Weight,cars_data$Mileage,method = "spearman")

kend_cor
spear_cor

# Kendall's rank correlation tau
# 
# data:  cars_data$Weight and cars_data$Mileage
# z = -20.49, p-value < 2.2e-16
# alternative hypothesis: true tau is not equal to 0
# sample estimates:
#        tau 
# -0.6940062 
# 
# 	Spearman's rank correlation rho
# 
# data:  cars_data$Weight and cars_data$Mileage
# S = 19700820, p-value < 2.2e-16
# alternative hypothesis: true rho is not equal to 0
# sample estimates:
#   rho 
# -0.8749474 